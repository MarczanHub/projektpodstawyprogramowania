package paczkomat;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Paczkomat {

    boolean exit = true;
    boolean isLogged = false;
    Scanner sc = new Scanner(System.in);
    Map<String, String> users = new HashMap<>();
    Map<String, String> usersPersonalInfo = new HashMap<>();

    public void run() {

        while (exit) {
            System.out.println("Witaj w �wiecie przesy�ek, co chcesz teraz zrobi�?\n");
            System.out.println(
                    "1. Zaloguj si� \n" +
                            "2. Stw�rz konto \n" +
                            "3. Wyjd� \n" +
                            "4. Poka� wszystkich u�ytkownik�w"
            );
            String decision = sc.next();
            switch (decision) {
                case "1" -> login();
                case "2" -> createAccount();
                case "3" -> exit();
                case "4" -> checkAllUsers();
                default -> System.out.println("Niepoprawna komenda");
            }
            isLoggedIn();
        }
    }

    private void exit() {
        System.out.println("Aplikacja zostanie zamkni�ta. Zapraszamy ponownie.");
        System.exit(0);
    }

    private void checkAllUsers() {
        if (users.isEmpty()) {
            System.out.println("Nie masz jeszcze dodanych �adnych u�ytkownik�w");
        } else {
            System.out.println(users.keySet());
        }
    }

    private void logout() {
        isLogged = false;
        System.out.println("Wylogowano");
    }

    private void createAccount() {
        boolean passwordIsCorrect = true;
        System.out.println("Prosz� poda� nazw� u�ytkownika");
        String username = sc.next();
        isUserInDB(username);
        String password = "";
        password = checkPassword(passwordIsCorrect, password);
        System.out.println("U�ytkownik " + username + " zosta� dodany do bazy danych");
        users.put(username, password);
    }

    private String checkPassword(boolean passwordIsCorrect, String password) {
        while (passwordIsCorrect) {
            System.out.println("Prosz� poda� has�o");
            password = sc.next();
            System.out.println("Prosz� powt�rzy� has�o");
            String secondPass = sc.next();
            passwordIsCorrect = !password.equals(secondPass);
            if (passwordIsCorrect) {
                System.out.println("Podane has�a nie s� takie same, spr�buj ponownie");
            }
        }
        return password;
    }

    private void isUserInDB(String username) {
        if (users.containsKey(username)) {
            System.out.println("U�ytkownik o podanym loginie ju� istnieje w bazie danych");
            createAccount();
        }
    }

    private void login() {
        System.out.println("Podaj login");
        String login = sc.next();
        System.out.println("Podaj has�o");
        String pass = sc.next();
        String passFromUsers = users.get(login);
        findUserInDB(login, pass, passFromUsers);
    }

    private void findUserInDB(String login, String pass, String passFromUsers) {
        if (pass.equals(passFromUsers)) {
            System.out.println("Jeste� zalogowany jako: " + login);
            isLogged = !isLogged;
        } else {
            System.out.println("Podany login lub has�o jest niepoprawne");
        }
    }

    private void showPackageParameters() {
        HashMap<String, String> packageParameters = new HashMap<>();
        packageParameters.put("Gabaryt A ", " wymiary: 80 x 380 x 640 mm");
        packageParameters.put("Gabaryt B ", " wymiary: 190 x 380 x 640 mm");
        packageParameters.put("Gabaryt C ", " wymiary: 410 x 380 x 640 mm");
        System.out.println(packageParameters);
    }

    private void isLoggedIn() {
        while (isLogged) {
            System.out.println("\nPanel u�ytkownika:\n");
            System.out.println(
                    "1. Edytuj moje dane\n" +
                            "2. Wy�wietl moje dane osobowe\n" +
                            "3. Nadaj przesy�k�\n" +
                            "4. Odbierz przesy�k�\n" +
                            "5. Wyloguj si�\n");
            String decision = sc.next();
            switch (decision) {
                case "1" -> editMyInfo();
                case "2" -> checkMyInfo();
                case "3" -> sendPackage();
                case "4" -> receivePackage();
                case "5" -> logout();
                default -> System.out.println("Niepoprawna komenda");
            }
        }
    }

    private void checkMyInfo() {
        if (usersPersonalInfo.isEmpty()) {
            System.out.println("Baza danych nie zosta�a jeszcze zaktualizowana.");
        } else {
            System.out.println("Dane u�ytkownika: " + usersPersonalInfo.keySet());
        }
    }

    private void sendPackage() {

        if (usersPersonalInfo.isEmpty()) {
            System.out.println("Najpierw uzupe�nij swoje dane!");
            isLoggedIn();
        } else {

            System.out.println("Oto dost�pne wymiary przesy�ek:\n");
            showPackageParameters();
            System.out.println("\nWybierz gabaryt przesy�ki (A, B lub C): ");

            double price = 0;
            String userDecision = sc.next();

            if (userDecision.equalsIgnoreCase("A")) {
                price = 14.69;
                System.out.println("Wybra�e� przesy�k� o maksymalnych wymiarach: 80 x 380 x 640 mm. Cena przesy�ki wynosi: " + price + " z�." +
                        "\n\n Aby zatwierdzi� wprowad� 'confirm' lub wprowad� 'back', je�li chcesz powr�ci� do panelu u�ytkownika.");
            } else if (userDecision.equalsIgnoreCase("B")) {
                price = 15.69;
                System.out.println("Wybra�e� przesy�k� o maksymalnych wymiarach: 190 x 380 x 640 mm. Cena przesy�ki wynosi " + price + " z�." +
                        "\n\nAby zatwierdzi� wprowad� 'confirm' lub wprowad� 'back', je�li chcesz powr�ci� do panelu u�ytkownika.");
            } else if (userDecision.equalsIgnoreCase("C")) {
                price = 17.39;
                System.out.println("Wybra�e� przesy�k� o maksymalnych wymiarach: 410 x 380 x 640 mm. Cena przesy�ki wynosi " + price + " z�." +
                        "\n\nAby zatwierdzi� wprowad� 'confirm' lub wprowad� 'back', je�li chcesz powr�ci� do panelu u�ytkownika.");
            } else {
                System.out.println("Niepoprawna komenda");
            }

            userDecision = sc.next();
            if (userDecision.equalsIgnoreCase("confirm")) {
                enterReceiverData();
                System.out.println("\nKoszt przesy�ki wynosi: " + price + " z�.\n");
            } else if (userDecision.equalsIgnoreCase("back")) {
                isLoggedIn();
            } else {
                System.out.println("Niepoprawna komenda");
            }
            System.out.println(
                    "1. Zap�a� za nadanie przesy�ki\n" +
                            "2. Anuluj nadanie przesy�ki/Wr�� do panelu u�ytkownika\n");
            String decision = sc.next();
            switch (decision) {
                case "1" -> payForPackage();
                case "2" -> isLoggedIn();
            }
            if (decision.equals("1")) {
                System.out.println("Dzi�kujemy za skorzystanie z naszych us�ug :)");
            }
        }
    }

    private void receivePackage() {
        int code = packagePickupCodeGenerator();
        if (usersPersonalInfo.isEmpty()) {
            System.out.println("Najpierw uzupe�nij swoje dane!");
            isLoggedIn();
        } else {
            System.out.println("Oto Tw�j unikalny kod odbioru:");
            System.out.println(code);
            System.out.println("\nWprowad� sw�j numer telefonu:");
        }
        sc.nextLine();
        String mobileNumber = sc.nextLine();
        while (!validateMobileNumber(mobileNumber)) {
            System.out.println("Podany numer telefonu jest niepoprawny");
            System.out.println("\nWprowad� sw�j numer telefonu:");
            mobileNumber = sc.nextLine();
            if (validateMobileNumber(mobileNumber)) {
                System.out.println("Numer telefonu jest poprawny");
                System.out.println("Podaj kod odbioru");
            }
        }

        try {
            int userCodeInput = sc.nextInt();
            while (userCodeInput != code) {
                System.out.println("Podany kod jest niepoprawny");
                System.out.println("Podaj kod odbioru:");
                userCodeInput = sc.nextInt();
                if (userCodeInput == code && validateMobileNumber(mobileNumber)) {
                    System.out.println("Wprowadzony kod jest poprawny.\n");
                    System.out.println("Oto Twoja przesy�ka.\nDzi�kujemy za skorzystanie z naszych us�ug :)");
                    isLoggedIn();
                }
            }
        } catch (Exception exception) {
            System.out.println("Kod nie zawiera innych znak�w, ni� cyfry.");
            System.out.println("Nast�pi przeniesienie do panelu u�ytkownika.");
        }
    }

    private int packagePickupCodeGenerator() {
        return (int) Math.round(Math.random() * 10000 + 1);
    }

    private boolean validateMobileNumber(String mobileNumber) {
        char[] chars = mobileNumber.toCharArray();
        if (chars.length == 9 && Character.isDigit(chars[0]) && chars[0] == '5' || chars[0] == '6'
                || chars[0] == '7' || chars[0] == '8' || chars[0] == '9' && Character.isDigit(chars[1])
                && Character.isDigit(chars[2]) && Character.isDigit(chars[3]) && Character.isDigit(chars[4])
                && Character.isDigit(chars[5]) && Character.isDigit(chars[6]) && Character.isDigit(chars[7])
                && Character.isDigit(chars[8])) {
            return true;
        } else {
            return false;
        }
    }

    private void payForPackage() {
        System.out.println("Twoja przesy�ka zosta�a op�acona!");
        System.out.println("Przesy�ka zosta�a nadana i teraz czeka na odbi�r przez pracownika firmy kurierskiej.");
    }

    private void enterReceiverData() {

        System.out.println("Wprowad� dane odbiorcy: ");
        String[] receiverData = new String[7];
        System.out.print("\tImi�: ");
        receiverData[0] = sc.next();
        System.out.print("\tNazwisko: ");
        receiverData[1] = sc.next();
        System.out.print("\tMiasto: ");
        receiverData[2] = sc.next();
        System.out.print("\tUlica: ");
        receiverData[3] = sc.next();
        System.out.print("\tNumer domu/Numer mieszkania: ");
        receiverData[4] = sc.next();
        System.out.print("\tKod pocztowy: ");
        receiverData[5] = sc.next();
        System.out.print("\tNumer telefonu: ");
        receiverData[6] = sc.next();

        System.out.println("\nAby zatwierdzi� wprowad� 'confirm' lub wprowad� 'back', je�li chcesz powr�ci� do panelu u�ytkownika.");
        String userDecision = sc.next();

        if (userDecision.equalsIgnoreCase("confirm")) {
            showReceiverInfo(receiverData);
        } else if (userDecision.equalsIgnoreCase("back")) {
            isLoggedIn();
        } else {
            System.out.println("Niepoprawna komenda");
        }
    }

    private void showReceiverInfo(String[] receiverData) {
        System.out.print("\t - Imi�: " + receiverData[0] + "\n");
        System.out.print("\t - Nazwisko: " + receiverData[1] + "\n");
        System.out.print("\t - Miasto: " + receiverData[2] + "\n");
        System.out.print("\t - Ulica: " + receiverData[3] + "\n");
        System.out.print("\t - Numer domu/Numer mieszkania: " + receiverData[4] + "\n");
        System.out.print("\t - Kod pocztowy: " + receiverData[5] + "\n");
        System.out.print("\t - Numer telefonu: " + receiverData[6] + "\n");
    }

    private void editMyInfo() {
        String[] database = new String[6];

        System.out.print("\t - Imi�: ");
        database[0] = sc.next();
        System.out.print("\t - Nazwisko: ");
        database[1] = sc.next();
        System.out.print("\t - Miasto: ");
        database[2] = sc.next();
        System.out.print("\t - Ulica: ");
        database[3] = sc.next();
        System.out.print("\t - Numer domu/Numer mieszkania: ");
        database[4] = sc.next();
        System.out.print("\t - Kod pocztowy: ");
        database[5] = sc.next();

        System.out.println("\nAby zatwierdzi� wprowad� 'confirm' lub wprowad� 'back', je�li chcesz powr�ci� do panelu u�ytkownika.");

        String userDecision = sc.next();
        if (userDecision.equalsIgnoreCase("confirm")) {
            showMyInfo(database);
        } else if (userDecision.equalsIgnoreCase("back")) {
            isLoggedIn();
        } else {
            System.out.println("Niepoprawna komenda");
        }
    }

    private void showMyInfo(String[] database) {

        System.out.print("\t - Imi�: " + database[0] + "\n");
        System.out.print("\t - Nazwisko: " + database[1] + "\n");
        System.out.print("\t - Miasto: " + database[2] + "\n");
        System.out.print("\t - Ulica: " + database[3] + "\n");
        System.out.print("\t - Numer domu/Numer mieszkania: " + database[4] + "\n");
        System.out.print("\t - Kod pocztowy: " + database[5] + "\n");
        usersPersonalInfo.put(Arrays.toString(database), String.valueOf(users.keySet()));
    }
}
